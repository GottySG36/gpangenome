module bitbucket.org/GottySG36/gpangenome

go 1.13

require (
	bitbucket.org/GottySG36/blast/v2 v2.5.0
	bitbucket.org/GottySG36/pangenome v0.4.0
	bitbucket.org/GottySG36/utils v0.3.1
)
