// Utility to filter a Blast and extract the Core, Accessory and Specific genes

package main

import (
    "flag"
    "fmt"
    "log"
    "strings"
    "sync"
    "time"

    blast "bitbucket.org/GottySG36/blast/v2"
    "bitbucket.org/GottySG36/pangenome"
    "bitbucket.org/GottySG36/utils"
)

var (
    inp   = flag.String("i", "", "Input file paths, comma seperated")
    key   = flag.String("k", "taxID", "Key to use for selecting which to compare in pangenome. One of {taxID, sampleID}")
    txids = flag.String("taxids", "", "List of all taxids used in analysis, comma seperated")
    outd  = flag.String("o", "", "Output directory(ies) in which to write the results, comma seperated")
    columns = flag.String("cols", "", "List of columns found in the blast result files")
    sort = flag.String("fields", "", "Fields to sort, in order of importance (see full description below to know which are available)")
    sep     = flag.String("sep",   "|", "Field delimiter used in sequence header. Use with '-pos' to indicate where the sample name is located in the header")
    bla   = flag.String("b", "", "Output file paths for the resulting sorted blast, comma seperated. (only print summary if blank)")
    pan   = flag.Int("p", 0, "Toggle to print the resulting pangenome table")
    ident = flag.Float64("p-ident", 95, "Blastp minimum identity percentage")
    core  = flag.Float64("core", 95, "Core genes threshold")
    acc   = flag.Float64("accessory", 15, "Accessory genes threshold")
    filtKing = flag.String("filt-kingdom", "", "Terms to ignore in row 'Kingdom', comma seperated")
    filtNames = flag.String("filt-names", "", "Terms to ignore in row 'Scinames', comma seperated")
)
func main() {
    flag.Parse()
    if *inp == "" {
        log.Fatalf("Error -:- Input : No files specified. At least one file must be specified with option '-i'\n")
    }
    files := strings.Split(*inp, ",")
    out := []string{}
    if *outd != "" {
        out = strings.Split(*outd, ",")
    }
    if len(out) != 0 && len(out) != len(files) {
        log.Fatalf("Error -:- Outdir : Number of output directories does not match the number of input files\n")
    }
    if *txids == "" {
        log.Fatalf("Error -:- Taxids : No taxids specified. Taxids must be specified with option '-taxids'\nand should reflect those found in the blast analysis")
    }
    if *key != "taxID" && *key != "sampleID" {
        log.Fatalf("Error -:- Key : Must either be 'taxID' or 'sampleID'. Received '%v'\n", *key)
    }
    if *filtKing == "" {
        *filtKing = "SHOULDNEVERMATCH"
    }
    if *filtNames == "" {
        *filtNames = "SHOULDNEVERMATCH"
    }

    var wg sync.WaitGroup
    var lock sync.Mutex
    sortedBlast := make(map[string]blast.Blast, len(files))

    quit := make(chan struct{})
    go utils.Spinner("Loading", 100, quit)

    // Read blast file(s) and sort the entries
    c := strings.Split(*columns, ",")
    for _, file := range files {
        wg.Add(1)
        go func(file string, columns []string, order string) {
            defer wg.Done()
            // Loading
            b, err := blast.LoadBlast(file, columns)
            if err != nil {
               log.Fatalf("Error -:- load_blast : %v\n", err)
            }
            srt, err := blast.ListSortClosures(order)
            if err != nil {
                log.Fatalf("Error -:- ListSortClosures : %v\n", err)
            }


            // Sorting then 
            // Keep only the best matches for each sequence
            blast.OrderedBy(srt).Sort(b)
            e, _, err := blast.ExtractBest(b, *ident, *filtKing, *filtNames)
                        if err != nil {
                log.Fatalf("Error -:- ExtractBest : %v\n", err)
            }

            srt2 , err := blast.ListSortClosures("qseqid")
            if err != nil {
                log.Fatalf("Error -:- ListSortClosures : qseqid : %v\n", err)
            }

            // Sorting by Query name for later steps
            blast.OrderedBy(srt2).Sort(e)

            lock.Lock()
            // Store individual results for traceability
            sortedBlast[file] = e
            lock.Unlock()

        }(file, c, *sort)
    }
    wg.Wait()
    close(quit)
    time.Sleep(250 * time.Millisecond)
    fmt.Printf(" Done!\n\n")

    tax := make(pangenome.Taxids)
    for i, val := range strings.Split(*txids, ",") {
        tax[val] = i
    }
    pangenomes := make(map[string]pangenome.Pangenome, len(files))

    quit = make(chan struct{})
    go utils.Spinner("Getting pangenome", 100, quit)
    for _, file := range files {
        wg.Add(1)
        go func(file string, t pangenome.Taxids, core, acc float64, key, sep string) {
            defer wg.Done()
            pang, err := sortedBlast[file].GetPangenome(tax, core, acc, key, sep)
            if err != nil {
                log.Fatalf("Error -:- GetPangenome : %v\n", err)
            }
            lock.Lock()
            // Store individual results for traceability
            pangenomes[file] = pang
            lock.Unlock()
        }(file, tax, *core, *acc, *key, *sep)
    }
    wg.Wait()
    close(quit)
    time.Sleep(250 * time.Millisecond)
    fmt.Printf(" Done!\n\n")

    summaries := make(map[string]pangenome.Summary)
    for _, file := range files {
        summaries[file] = pangenomes[file].Summarize()
    }

    if *outd != ""{
        // Writing blasts to file
        quit := make(chan struct{})
        for i, file := range files {
            wg.Add(1)
            go func(f, o string) {
                defer wg.Done()
                sortedBlast[f].Write(o, "sorted_blast.csv")
            }(file, out[i])
        }
        wg.Wait()
        close(quit)

        // Writing Pangenomes to file
        quit = make(chan struct{})
        for i, file := range files {
            wg.Add(1)
            go func(f, o string, t pangenome.Taxids) {
                defer wg.Done()
                pangenomes[f].Write(o, t)
            }(file, out[i], tax)
       }
        wg.Wait()
        close(quit)

        // Writing summaries to file
        quit = make(chan struct{})
        for i, file := range files {
            wg.Add(1)
            go func(f, o string) {
                defer wg.Done()
                summaries[f].Write(o)
            }(file, out[i])
       }
        wg.Wait()
        close(quit)
    } else {
        for f, summ := range summaries {
            fmt.Printf("Summary for blast file %v\n", f)
            summ.Print()
            fmt.Println()
        }
    }
}
